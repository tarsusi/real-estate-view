module.exports = {
  arrowParens: 'always',
  bracketSameLine: true,
  jsxBracketSameLine: true,
  bracketSpacing: false,
  singleQuote: true,
  tabWidth: 4,
  trailingComma: 'all',
  printWidth: 80,
};
